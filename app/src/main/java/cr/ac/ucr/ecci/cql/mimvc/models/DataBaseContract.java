package cr.ac.ucr.ecci.cql.mimvc.models;

import android.provider.BaseColumns;

public final class DataBaseContract {
    // Para asegurar que no se instancie la clase hacemos el constructor privado
    private DataBaseContract() {}
    // Definimos una clase interna que define las tablas y columnas
    // Implementa la interfaz BaseColumns para heredar campos estandar del marco de Android _ID

    public static class DataBaseEntry implements BaseColumns {
        // Clase Persona
        public static final String TABLE_NAME_PERSONA = "Persona";
        // private String identificacion; Utilizamos DataBaseEntry._ID de BaseColumns
        // private String Nombre
        public static final String COLUMN_NAME_NOMBRE = "nombre";
        // Clase Estudiante
        public static final String TABLE_NAME_ESTUDIANTE = "Estudiante";
        // private String carnet;
        public static final String COLUMN_NAME_CARNET = "carnet";
        public static final String COLUMN_NAME_PROMEDIO_PONDERADO = "promedioPonderado";
    }

    // Construir las tablas de la base de datos
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP = ",";

    // Creacion de tablas Persona, Estudiante
    public static final String SQL_CREATE_PERSONA =
            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_PERSONA + " (" +
                    DataBaseEntry._ID + TEXT_TYPE + "PRIMARY KEY," +
                    DataBaseEntry.COLUMN_NAME_NOMBRE + TEXT_TYPE + " )";

    public static final String SQL_DELETE_PERSONA =
            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_PERSONA;

    public static final String SQL_CREATE_ESTUDIANTE =
            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_ESTUDIANTE + " (" +
                    DataBaseEntry._ID + TEXT_TYPE + "PRIMARY KEY," +
                    DataBaseEntry.COLUMN_NAME_CARNET + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_PROMEDIO_PONDERADO + REAL_TYPE + COMMA_SEP +
                    "FOREIGN KEY(" + DataBaseEntry._ID + ") REFERENCES " +
                    DataBaseEntry.TABLE_NAME_PERSONA + "(" + DataBaseEntry._ID + "))";

    public static final String SQL_DELETE_ESTUDIANTE = "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_ESTUDIANTE;
}
