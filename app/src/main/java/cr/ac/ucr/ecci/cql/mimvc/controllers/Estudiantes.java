package cr.ac.ucr.ecci.cql.mimvc.controllers;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import cr.ac.ucr.ecci.cql.mimvc.models.Estudiante;

public class Estudiantes {
    private Context mContext;
    private List<Estudiante> mEstudiantes;
    public Estudiantes(Context context) {
        mContext = context;
    }
    public List<Estudiante> getEstudiantes() {
        return mEstudiantes;
    }
    public void setEstudiantes(List<Estudiante> mEstudiantes) {
        this.mEstudiantes = mEstudiantes;
    }
    public void leerEstudiantes() {
        Estudiante mEstudiante1 = new Estudiante();
        mEstudiante1.leer(mContext, "100010001");

        Estudiante mEstudiante2 = new Estudiante();
        mEstudiante2.leer(mContext, "100010002");

        Estudiante mEstudiante3 = new Estudiante();
        mEstudiante3.leer(mContext, "100010003");

        mEstudiantes = new ArrayList<Estudiante>();
        mEstudiantes.add(mEstudiante1);
        mEstudiantes.add(mEstudiante2);
        mEstudiantes.add(mEstudiante3);
    }

    public void crearEstudiantes() {
        // Dummy codes para estudiantes
        Estudiante mEstudiante1 = new Estudiante("100010001", "Juan Perez Perez", "A99100", 8.0);
        mEstudiante1.insertar(mContext);
        Estudiante mEstudiante2 = new Estudiante("100010002", "Albert Einstein Tesla", "A99101", 9.8);
        mEstudiante2.insertar(mContext);
        Estudiante mEstudiante3 = new Estudiante("100010003", "Laurie Pascal Tunning", "A99102", 9.5);
        mEstudiante3.insertar(mContext);
    }
}

