package cr.ac.ucr.ecci.cql.mimvc.controllers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cr.ac.ucr.ecci.cql.mimvc.R;
import cr.ac.ucr.ecci.cql.mimvc.models.Estudiante;

public class LazyAdapter extends BaseAdapter {
    private List<Estudiante> mData;
    private Context mContext;
    public LazyAdapter(Estudiantes data, Context context) {
        mData = data.getEstudiantes();
        mContext = context;
    }

    public int getCount() {
        return mData.size();
    }
    public Object getItem(int position) {
        return position;
    }
    public long getItemId(int position) {
        return position;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = null;

        Estudiante mEstudiante = mData.get(position);

        rowView = inflater.inflate(R.layout.list_row, parent, false);

        TextView nombre = (TextView)rowView.findViewById(R.id.nombre);
        TextView carne = (TextView)rowView.findViewById(R.id.carne);
        ImageView imagen = (ImageView)rowView.findViewById(R.id.imagen);
        nombre.setText(mEstudiante.getNombre());
        carne.setText(mEstudiante.getCarnet());
        imagen.setImageResource(R.drawable.i01);
        return rowView;
    }
}