package cr.ac.ucr.ecci.cql.mimvc.views;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import cr.ac.ucr.ecci.cql.mimvc.R;
import cr.ac.ucr.ecci.cql.mimvc.controllers.Estudiantes;
import cr.ac.ucr.ecci.cql.mimvc.controllers.LazyAdapter;

// En nuestro ejemplo la Actividad Estudiantes representa parte del View
// Este solicita la información de los estudiantes y la presenta al usuario
public class EstudiantesActivity extends AppCompatActivity {
    private ListView mListView;
    private Estudiantes mEstudiantes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estudiantes);
        mEstudiantes = new Estudiantes(getApplicationContext());

        // Dummy codes para estudiantes
        mEstudiantes.crearEstudiantes();

        mEstudiantes.leerEstudiantes();
        mListView = (ListView) findViewById(R.id.miLista);

        LazyAdapter mAdapter = new LazyAdapter(mEstudiantes, this);
        mListView.setAdapter(mAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO: la implementacion se hace en una clase del Controller
                Toast.makeText(getApplicationContext(), "Item: " + mEstudiantes.getEstudiantes().get(position).getCarnet(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}

