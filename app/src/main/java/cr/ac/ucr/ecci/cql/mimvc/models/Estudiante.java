package cr.ac.ucr.ecci.cql.mimvc.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import java.util.ArrayList;
import java.util.List;

public class Estudiante extends Persona {
    private String carnet;
    private double promedioPonderado;
    public Estudiante() {
        super();
    }
    public Estudiante(String identificacion, String nombre, String carnet, double
            promedioPonderado) {
        super(identificacion, nombre);
        this.carnet = carnet;
        this.promedioPonderado = promedioPonderado;
    }
    public String getCarnet() {
        return carnet;
    }
    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }
    public double getPromedioPonderado() {
        return promedioPonderado;
    }
    public void setPromedioPonderado(double promedioPonderado) {
        this.promedioPonderado = promedioPonderado;
    }
    public Estudiante(Parcel in, String carnet, int carreraBase, double promedioPonderado) {
        super(in);
        this.carnet = carnet;
        this.promedioPonderado = promedioPonderado;
    }
    // insertar un estudiante en la base de datos
    public long insertar(Context context) {
        // inserta la persona antes del estudiante
        long newRowId = super.insertar(context);
        // si inserta la Persona inserto el estudiante
        if (newRowId > 0) {
        // usar la clase DataBaseHelper para realizar la operacion de insertar
            DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
            // Obtiene la base de datos en modo escritura
            SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
            // Crear un mapa de valores donde las columnas son las llaves
            ContentValues values = new ContentValues();
            values.put(
                    DataBaseContract.DataBaseEntry._ID, getIdentificacion());
            values.put(
                    DataBaseContract.DataBaseEntry.COLUMN_NAME_CARNET, getCarnet());
            values.put(
                    DataBaseContract.DataBaseEntry.COLUMN_NAME_PROMEDIO_PONDERADO,
                    getPromedioPonderado());
            // Insertar la nueva fila
            newRowId = db.insert(DataBaseContract.DataBaseEntry.TABLE_NAME_ESTUDIANTE, null,
                    values);
        }
        return newRowId;
    }
    // leer un estudiante desde la base de datos
    public void leer (Context context, String identificacion){
        // leer la persona antes del estudiante
        super.leer(context, identificacion);
        // usar la clase DataBaseHelper para realizar la operacion de select
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        // Obtiene la base de datos en modo lectura
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        // Define cuales columnas quiere solicitar // en este caso todas las de la clase
        String[] projection = {
                DataBaseContract.DataBaseEntry._ID,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_CARNET,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_PROMEDIO_PONDERADO
        };
        // Filtro para el WHERE
        String selection = DataBaseContract.DataBaseEntry._ID + " = ?";
        String[] selectionArgs = {identificacion};
        // Resultados en el cursor
        Cursor cursor = db.query(
                DataBaseContract.DataBaseEntry.TABLE_NAME_ESTUDIANTE,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        // recorrer los resultados y asignarlos a la clase // aca podria implementarse un ciclo si es necesario
        if (cursor.moveToFirst() && cursor.getCount() > 0) {
            setCarnet(cursor.getString(cursor.getColumnIndexOrThrow(
                    DataBaseContract.DataBaseEntry.COLUMN_NAME_CARNET)));
            setPromedioPonderado(cursor.getDouble(cursor.getColumnIndexOrThrow(
                    DataBaseContract.DataBaseEntry.COLUMN_NAME_PROMEDIO_PONDERADO)));
        }
    }

    public List<Estudiante> leerEstudiantes() {
        List<Estudiante> resultado;
        resultado = new ArrayList<Estudiante>();
        // TODO: implementar la lectura de todos los estudiantes y personas
        return resultado;
    }

    // eliminar un estudiante desde la base de datos
    public void eliminar (Context context, String identificacion) {
        // usar la clase DataBaseHelper para realizar la operacion de eliminar
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        // Obtiene la base de datos en modo escritura
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        // Define el where para el borrado
        String selection = DataBaseContract.DataBaseEntry._ID + " LIKE ?";
        // Se detallan los argumentos
        String[] selectionArgs = {identificacion};
        // Realiza el SQL de borrado
        db.delete(DataBaseContract.DataBaseEntry.TABLE_NAME_ESTUDIANTE, selection, selectionArgs);
        // eliminar la persona despues del estudiante
        super.eliminar(context, identificacion);
    }

    // actualizar un estudiante en la base de datos
    public int actualizar(Context context) {
        // usar la clase DataBaseHelper para realizar la operacion de actualizar
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        // Obtiene la base de datos
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        // Crear un mapa de valores para los datos a actualizar
        ContentValues values = new ContentValues();
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_CARNET, getCarnet());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PROMEDIO_PONDERADO, getPromedioPonderado());

        // Criterio de actualizacion
        String selection = DataBaseContract.DataBaseEntry._ID + " LIKE ?";

        // Se detallan los argumentos
        String[] selectionArgs = {getIdentificacion()};

        // Actualizar la base de datos
        int contador = db.update(DataBaseContract.DataBaseEntry.TABLE_NAME_ESTUDIANTE, values, selection, selectionArgs);

        // actualizar la persona despues del estudiante
        contador += super.actualizar(context);
        return contador;
    }
}